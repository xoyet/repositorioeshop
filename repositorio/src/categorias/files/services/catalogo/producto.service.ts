import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  private _url: string;

  onProductoChanged: BehaviorSubject<any>;

  /**
   * Producto service constructor
   */
  constructor(private _http: HttpClient) {
    //this._url = environment.urlApi;
    this.onProductoChanged = new BehaviorSubject({});
  }

  /**
   * Get producto
   */
  getProducto(id: string): Promise<any> {
    //const url = `${this._url}/producto`;

    const productos = [
        {
            id: '0ce291c0-a3c0-11ea-bb37-0242ac130002',
            marca: 'elit',
            nombre: 'culpa',
            imagen: 'http://placehold.it/400x200',
            precioReal: 39658,
            precioOferta: 11109,
            descripcion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nisl ligula, vestibulum a malesuada id, feugiat et lectus. Nam semper lectus sit amet vehicula gravida. Donec volutpat, erat vel dapibus laoreet, mauris tellus convallis augue, quis pulvinar nunc lorem sed tortor. Cras nec nunc blandit, varius eros a, lacinia ipsum. Sed et orci quis elit egestas cursus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam at ex massa. Nunc cursus lectus blandit felis blandit lobortis. Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor, ac lobortis magna.',
            detalle: [
              { nombre: 'Color', contenido: 'Blanco' },
              { nombre: 'Modelo', contenido: 'PF3R92' },
              { nombre: 'Alto', contenido: '20cm' },
              { nombre: 'Ancho', contenido: '40cm' },
              { nombre: 'Profundidad', contenido: '10cm' }
            ],
            categorias: [
              'Tecnología',
              'Instrumentos'
            ]
        },
        {
            id: '0ce291c0-a3c0-11ea-bb37-0242ac139238',
            marca: 'labore',
            nombre: 'officia',
            imagen: 'http://placehold.it/400x200',
            precioReal: 14937,
            precioOferta: 31751,
            descripcion: 'Proin nisl ligula, vestibulum a malesuada id, feugiat et lectus. Nam semper lectus sit amet vehicula gravida. Donec volutpat, erat vel dapibus laoreet, mauris tellus convallis augue, quis pulvinar nunc lorem sed tortor. Cras nec nunc blandit, varius eros a, lacinia ipsum. Sed et orci quis elit egestas cursus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam at ex massa. Nunc cursus lectus blandit felis blandit lobortis. Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor, ac lobortis magna.',
            detalle: [
              { nombre: 'Color', contenido: 'Negro' },
              { nombre: 'Talla', contenido: 'M' },
              { nombre: 'Material', contenido: 'Algodón' }
            ],
            categorias: [
              'Moda',
              'Vestuario',
              'Mujer'
            ]
        },
        {
            id: '0ce291c0-a3c0-11ea-cc37-0242ad430002',
            marca: 'commodo',
            nombre: 'irure',
            imagen: 'http://placehold.it/400x200',
            precioReal: 16784,
            precioOferta: 23369,
            descripcion: 'Nunc cursus lectus blandit felis blandit lobortis. Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor, ac lobortis magna.',
            detalle: [
              { nombre: 'Origen', contenido: 'Corea' }
            ],
            categorias: [
              'Tecnología'
            ]
        },
        {
            id: '0ce291c0-z2c0-11ea-bb37-0242ac130002',
            marca: 'elit',
            nombre: 'fugiat',
            imagen: 'http://placehold.it/400x200',
            precioReal: 25737,
            precioOferta: 16923,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Estilo', contenido: 'Casual' },
              { nombre: 'Origen', contenido: 'Corea' }
            ],
            categorias: [
              'Moda',
              'Vestuario',
              'Hombre'
            ]
        },
        {
            id: '0ce291c0-a3c0-11fe-bb37-0242ac130002',
            marca: 'commodo',
            nombre: 'incididunt',
            imagen: 'http://placehold.it/400x200',
            precioReal: 36462,
            precioOferta: 2437,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 1'
            ]
        },
        {
            id: '0cd291c3-a3c0-11ea-bb37-0242ac130002',
            marca: 'cillum',
            nombre: 'incididunt',
            imagen: 'http://placehold.it/400x200',
            precioReal: 33989,
            precioOferta: 24975,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 2',
              'Categoría 3'
            ]
        },
        {
            id: '0ce291c0-a3c0-hhea-bb37-0242ac130095',
            marca: 'deserunt',
            nombre: 'pariatur',
            imagen: 'http://placehold.it/400x200',
            precioReal: 13880,
            precioOferta: 22024,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 3'
            ]
        },
        {
            id: '0rt291c0-a3c0-11ea-bb37-0242ac130076',
            marca: 'proident',
            nombre: 'deserunt',
            imagen: 'http://placehold.it/400x200',
            precioReal: 18533,
            precioOferta: 37306,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 2'
            ]
        },
        {
            id: '0ce291c0-a3c0-11ea-jh37-0242gc110002',
            marca: 'ex',
            nombre: 'laborum',
            imagen: 'http://placehold.it/400x200',
            precioReal: 9063,
            precioOferta: 21791,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 1',
              'Categoría 2',
              'Categoría 3'
            ]
        },
        {
            id: '0ce281c1-a3c0-11ea-bb37-0242ac130002',
            marca: 'tempor',
            nombre: 'exercitation',
            imagen: 'http://placehold.it/400x200',
            precioReal: 12772,
            precioOferta: 30535,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 3'
            ]
        },
        {
            id: 'cce291k0-a3c0-11ea-bb37-0242ac130002',
            marca: 'et',
            nombre: 'eu',
            imagen: 'http://placehold.it/400x200',
            precioReal: 28334,
            precioOferta: 22992,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 1',
              'Categoría 3'
            ]
        },
        {
            id: '0ce291c0-a3c0-1111-bb37-0242ac130005',
            marca: 'ex',
            nombre: 'ullamco',
            imagen: 'http://placehold.it/400x200',
            precioReal: 37495,
            precioOferta: 16238,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 2'
            ]
        },
        {
            id: '0ce291c0-a3c0-11ea-bbcc-02as2ac130002',
            marca: 'voluptate',
            nombre: 'proident',
            imagen: 'http://placehold.it/400x200',
            precioReal: 36983,
            precioOferta: 17603,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Categoría 2',
              'Categoría 3'
            ]
        },
        {
            id: '0ce291c0-a3c0-11ea-bb37-0242ac139494',
            marca: 'ut',
            nombre: 'nulla',
            imagen: 'http://placehold.it/400x200',
            precioReal: 8788,
            precioOferta: 25293,
            descripcion: 'Nam id mattis ligula. Maecenas non ultricies orci. Aliquam id nisl mauris. Vestibulum vitae dui at nibh finibus ultrices vel at dolor. Vivamus augue metus, luctus ut libero et, convallis pretium mauris. Suspendisse at dictum tortor',
            detalle: [
              { nombre: 'Característica', contenido: 'Lorem ipsum' }
            ],
            categorias: [
              'Tecnología'
            ]
        }
    ];

    const producto = productos.find(prod => prod.id === id);

    return new Promise((resolve, reject) => {
        this.onProductoChanged.next(producto);
        /*
        this._http.get(url).subscribe((response: any) => {
            this.onProductoChanged.next(response.results);
            resolve(response.results);
          }, reject);*/
    });
  }
}