export interface reglasCategorias {
    general: boolean;
    carrito: boolean;
    crud_productos: boolean;
    crud_categorias: boolean;
}