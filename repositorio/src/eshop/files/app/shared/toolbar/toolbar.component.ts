import { Component<%if(categorias) {%>, OnInit, OnDestroy<%}%> } from '@angular/core';<%if(categorias) {%>
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CategoriasService } from 'src/app/services/catalogo/categorias.service';<%}%>

@Component({
  selector: 'app-toolbar',
  templateUrl: 'toolbar.component.html',
  styleUrls: ['toolbar.component.scss'],
})
export class ToolbarComponent <%if(categorias) {%>implements OnInit, OnDestroy<%}%> {
  <%if(categorias) {%>
  public categorias;

  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   */
  constructor(private _categoriasService: CategoriasService) {
    this.categorias = this._categoriasService.getCategorias();
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Subscribe to categorias
    this._categoriasService.onCategoriasChanged
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(categorias => {
        this.categorias = categorias;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }<%}%>
}