import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { normalize, strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicContext,
  Tree,
  chain,
  externalSchematic,
  SchematicsException,
  Source,
  template,
  url,
  apply,
  move,
  mergeWith
} from '@angular-devkit/schematics';
/*import { InsertChange } from '@schematics/angular/utility/change';*/
import { getWorkspace } from '@schematics/angular/utility/workspace';
import { getPackageJsonDependency, addPackageJsonDependency } from '@schematics/angular/utility/dependencies';
import { findModule } from '@schematics/angular/utility/find-module';
/*import {
  addImportToModule,
  //addEntryComponentToModule,
  //addExportToModule,
} from '@schematics/angular/utility/ast-utils';*/

//import * as ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';

import { reglasEshop } from './schema';

export function configurarEshop(_options: reglasEshop): Rule {
  return async (tree: Tree, _context: SchematicContext) => {
    const workspace = await getWorkspace(tree);
    const proyecto = workspace.projects.get(_options.proyecto as string);
    console.log(workspace)
    console.log(proyecto)
    const modulo = findModule(tree, 'src/app');

    return chain([
      agregarDependencias(),
      eliminarModuloDefecto(modulo),
      eliminarRutasDefecto(),
      eliminarComponenteApp(),
      eliminarEstilosDefecto(),
      eliminarIndexDefecto(),
      crearModuloPersonalizado(_options)
    ]);
  };
}

function agregarDependencias(): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Añadiendo dependencias básicas al proyecto');
    const coreDep = getPackageJsonDependency(tree, '@angular/core');

    if (coreDep === null) {
      throw new SchematicsException('No se encuentra la version de @angular/core en package.json');
    }

    const dependencias = [
      {
        ...coreDep,
        name: '@angular/flex-layout',
        version: '9.0.0-beta.31'
      },
      {
        ...coreDep,
        name: '@angular/cdk'
      },
      {
        ...coreDep,
        name: '@angular/material'
      },
      {
        ...coreDep,
        name: 'ngx-progressbar',
        version: '6.0.3'
      }
    ]

    dependencias.map(dependencia => addPackageJsonDependency(tree, dependencia));

    //Instalar las dependencias (necesario solo por @angular/material, para hacer uso de su Schematic)
    _context.addTask(new NodePackageInstallTask());

    //Ejecutar el Schematic de @angular/material
    externalSchematic('@angular/material', 'install',
    { 'skipPackageJson': false, 'theme': 'deeppurple-amber', 'typography': true, 'animations': true });

    return tree;
  };
}

function eliminarModuloDefecto(ruta: string): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Eliminado app.module.ts por defecto');
    tree.delete(ruta);
    return tree;
  };
}

function crearModuloPersonalizado(_options: reglasEshop): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Creando módulo personalizado');
    const source: Source = url('./files');
    const transformedSource: Source = apply(source, [
      template({
        filename: 'src',
        ...strings,
        ..._options
      }),
      move(normalize('src'))
    ]);

    return mergeWith(transformedSource)(tree, _context);
  };
}

function eliminarRutasDefecto(): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Eliminando rutas por defecto');
    tree.delete('/src/app/app-routing.module.ts');
    return tree;
  };
}

function eliminarComponenteApp(): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Eliminando, app.component.html, app.component.ts y app.component.scss');
    //Eliminados para ser reemplazados:
    tree.delete('/src/app/app.component.ts');
    tree.delete('/src/app/app.component.html');
    tree.delete('/src/app/app.component.scss');

    //Eliminado definitivamente a menos que se añadan pruebas unitarias
    tree.delete('/src/app/app.component.spec.ts');
    return tree;
  }
}

function eliminarEstilosDefecto(): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Eliminando, styles.scss');
    //Eliminados para ser reemplazados:
    tree.delete('/src/styles.scss');
    return tree;
  }
}

function eliminarIndexDefecto(): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Eliminando, index.html');
    //Eliminados para ser reemplazados:
    tree.delete('/src/index.html');
    return tree;
  }
}


/*
function agregarModulos(rutaModulo: string): Rule {
  return (tree: Tree) => {
    const appModule = readIntoSourceFile(tree, rutaModulo);

    const classifiedName = strings.classify('hola') + strings.classify('chao');

    const declarationChanges = addImportToModule(appModule,
      rutaModulo,
      classifiedName,
      'relativo');

    const declarationRecorder = tree.beginUpdate(rutaModulo);
    for (const change of declarationChanges) {
      if (change instanceof InsertChange) {
        declarationRecorder.insertLeft(change.pos, change.toAdd);
      }
    }
    tree.commitUpdate(declarationRecorder);


    return tree;
  };
}*/

/*
function readIntoSourceFile(tree: Tree, modulo: string): ts.SourceFile {
  const text = tree.read(modulo);
  if (text === null) {
    throw new SchematicsException(`File ${modulo} does not exist.`);
  }

  const sourceText = text.toString('utf-8');

  return ts.createSourceFile(modulo, sourceText, ts.ScriptTarget.Latest, true);
}
*/